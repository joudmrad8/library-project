
import 'package:flutter/material.dart';
import 'package:library_project/view/Screen/Homepage/homeadmin.dart';
import 'package:library_project/view/Screen/Homepage/homepage.dart';
import 'package:library_project/view/Screen/Homepage/search.dart';
import 'package:library_project/view/Screen/auth/login.dart';
import 'package:library_project/view/Screen/auth/signup.dart';
import 'package:library_project/view/Screen/onboarding.dart';

import 'Core/Constant/routes.dart';

Map<String, Widget Function(BuildContext)> routes = {
  AppRoute.login: (context) => const Login(),
  AppRoute.onboarding: (context) => onBoarding(),
  AppRoute.signup: (context) => SignUp(),
  AppRoute.homepage: (context) => HomePage(),
  AppRoute.homepagead: (context) =>  HomePageAdmin(),

//  AppRoute.search: (context) => Search(),


};