import 'package:get/get.dart';

class MyTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {

    'ar': {
      '1': 'اختر اللغة ',
      "2": "مرحبًا بك وابدأً",
      '3':"أهلاً وسهلاً في مكتبتك",
      "4": "أوجد كتابك وقائمة كتبك المفضلة",
       "5": "أسهل طريقة لشراء واستعارة الكتب",
       "6": "تخطي",
      '7': 'تسجيل الدخول',
      '8': 'مرحباً بعودتك',
      '9':'اسم المستخدم',
      '10':'ادخل اسم المستخدم ',
      '11':'البريد الالكتروني',
      '12':'ادخل البريد الالكتروني',
      '13': 'كلمة المرور',
      '14': 'أدخل كلمة المرور',
      '15':'هل لديك حساب؟',
      "16":'تسجبل الدخول',
      "17":"غير صالح ",
      '18':"صالح",
      "19":"اسم المستخدم غير صالح",
      "confirm title":"تأكيد كلمة السر",
      "confirm":" أدخل كلمة السر مرة ثانية للتأكد"
    },


    'en': {
      '1': 'Choose Language',
      "2": "Welcome and get started",
      "3": "welcome in your library",
      "4": "Find your book and list your favourite",
      "5": "An easy way to buy and borrow books",
      "6": "Continue",
      '7': 'Signup ',
      '8': ' Welcome Back',
      '9':'Username',
      '10':'Enter Your Username',
      '11': 'Email',
      '12':'Enter your Email',
      '13': 'Password',
      '14': 'Enter Your Password',
      '15':" have an account ? ",
      "16":'Login',
      "17":"not valid",
      '18':"valid",
      "19":"not valid username",
      "confirm title":"password confirmation",
      "confirm":"confirm your password"


    },
  };
}
