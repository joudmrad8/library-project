import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:library_project/Core/Class/statuscodeReq.dart';
import 'package:library_project/Core/Function/checkInternet.dart';
import 'package:library_project/Core/Class/crud.dart';
import 'package:http/http.dart' as http;

class Crud {

  Future<Either <StatusRequest,Map>> postData(String linkurl,Map data) async{
    try {
      if (await checkInternet()) {
        var response = await http.post(Uri.parse(linkurl), body: data);
        print(response.statusCode);
        if (response.statusCode == 200 || response.statusCode == 201) {
          print(response.statusCode);
          Map responsebody = jsonDecode(response.body);
          return Right(responsebody);
        } else {
          return left(StatusRequest.serverfailure);
        }
      } else {
        return left(StatusRequest.offlinefailure);
      }
    }catch(_){
      return Left(StatusRequest.serverException);
    }
  }
}