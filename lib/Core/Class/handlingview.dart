
import 'package:flutter/material.dart';
import 'package:library_project/Core/Class/statuscodeReq.dart';
import 'package:lottie/lottie.dart';

import '../Constant/imageassets.dart';

class HandlingDataView extends StatelessWidget {
  final StatusRequest statusRequest;
  final Widget widget;
  const HandlingDataView(
      {Key? key, required this.statusRequest, required this.widget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return statusRequest == StatusRequest.loading
        ?   Center(child: Lottie.asset( AppImageassets.loading , width: 250 , height: 250))
        : statusRequest == StatusRequest.offlinefailure
        ? Center(child: Lottie.asset( AppImageassets.offline , width: 250 , height: 250))
        : statusRequest == StatusRequest.serverfailure
        ?Center(child: Lottie.asset( AppImageassets.server , width: 250 , height: 250))
        : statusRequest == StatusRequest.failure
        ? Center(child: Lottie.asset( AppImageassets.noData , width: 250 , height: 250 , repeat: true  ))
        : widget;
  }
}