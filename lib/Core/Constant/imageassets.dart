class AppImageassets{
  static const String rootImage="assets/images";
  static const String rootLottie="assets/lottie";

  static const String onboardingImageone="$rootImage/onboard.jpg";
  static const String onboardingImagetwo="$rootImage/onboardingtwo.jpg";
  static const String onboardingImagethree="$rootImage/onboardingthree.png";
  static const String searchImge="$rootLottie/search.json";
  static const String SignupImge="$rootLottie/signup (2).json";
  static const String loading = "$rootLottie/Loading.json";
  static const String offline = "$rootLottie/offline.json";
  static const String noData = "$rootLottie/nodata.json";
  static const String server = "$rootLottie/offline.json";
// static const String SignupImge="$rootLottie/signup.json";

}