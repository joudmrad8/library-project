import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/localaization/translation.dart';
import 'package:library_project/controller/SignUpController.dart';
import 'package:library_project/routes.dart';
import 'package:library_project/view/Screen/Homepage/homepage.dart';
import 'package:library_project/view/Screen/Homepage/search.dart';
import 'package:library_project/view/Screen/auth/signup.dart';
import 'package:library_project/view/Screen/langauge.dart';
import 'package:library_project/view/Screen/onboarding.dart';
import 'Core/Class/cashhelper.dart';
import 'Core/Class/crud.dart';
import 'Core/Constant/color.dart';
import 'Core/Services/services.dart';
import 'Core/localaization/changelocal.dart';
import 'package:http/http.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialServices();
  Widget? widget;

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    LocaleController controller =   Get.put(LocaleController()) ;
    Crud crud =Get.put(Crud());

    return GetMaterialApp(

      debugShowCheckedModeBanner: false,
      translations: MyTranslation(),
      locale: controller.language,
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: const TextTheme(
          headline1:
                   TextStyle(
                   fontWeight: FontWeight.bold,color: Appcolor.black, fontSize: 24,letterSpacing: 0.5),
          bodyText1: TextStyle(
                    height: 2, color: Appcolor.grey, fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
      primaryColor: Appcolor.primaryColor,

     // primarySwatch: Colors.teal ,
      ),

      home: HomePage(),

      routes: routes,
    );
  }
}



