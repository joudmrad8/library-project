import 'package:get/get.dart';
import 'package:library_project/Core/Constant/imageassets.dart';
import 'package:library_project/Data/Model/onboardingmodel.dart';
import '../../../controller/OnBoardingController.dart';

List <onBordingModel> onboardingList = [
  onBordingModel(title: "2".tr,
      body: "3".tr,
      image: AppImageassets.onboardingImageone),
  onBordingModel(title: "2".tr,
      body: "4".tr,
      image: AppImageassets.onboardingImagetwo),
  onBordingModel(title: "2".tr,
      body: "5".tr,
      image: AppImageassets.onboardingImagethree),
];
