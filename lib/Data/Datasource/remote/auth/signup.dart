import 'package:library_project/LinkApi.dart';

import '../../../../Core/Class/crud.dart';


class SignupData {
  Crud crud;
  SignupData(this.crud);
  PostData(String username,String email,String password ,String confpassword)  async{
    var response =await crud.postData(AppLink.register,{
      "username": username ,
      "email": email,
      "password": password,
      "C_password": confpassword,

    });
    return response.fold((l) => l, (r) => r);
  }
}