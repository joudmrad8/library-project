

import '../../../Core/Class/crud.dart';
import '../../../LinkApi.dart';

class HomeData {
  Crud crud;
  HomeData(this.crud);
  getData() async {
    var response = await crud.postData(AppLink.home, {

    });
    return response.fold((l) => l, (r) => r);
  }
}