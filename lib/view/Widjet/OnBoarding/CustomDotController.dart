import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/controller/OnBoardingController.dart';

import '../../../Core/Constant/color.dart';
import '../../../Data/Datasource/static/static.dart';

class CustomDotControllerOnBoarding extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  GetBuilder<OnBoardingControllerImp>(builder: (controller)=> Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ...List.generate(onboardingList.length,
              (index) => AnimatedContainer(
                margin: const EdgeInsets.only(right: 5),
                duration: const Duration(milliseconds: 800),
                width: controller.currentPage == index ? 20 : 5,
                height: 6,
                decoration: BoxDecoration(
                    color: Appcolor.primaryColor,
                    borderRadius: BorderRadius.circular(10)),
              ))
      ],
    ) );
  }

}