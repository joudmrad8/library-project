import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../Core/Constant/color.dart';
import '../../../Core/Constant/imageassets.dart';
import '../../../Data/Datasource/static/static.dart';
import '../../../Data/Model/onboardingmodel.dart';
import '../../../controller/OnBoardingController.dart';

class CustomSlider extends GetView<OnBoardingControllerImp>{
  const CustomSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      controller: controller.pageController,
      onPageChanged: (val)
    {
      controller.onPageChanged(val);
    },

      itemCount: onboardingList.length,
      itemBuilder: (count,i)=> Column(
        children: [
          const SizedBox(height: 80),
          Image.asset(
            onboardingList[i].image!,
            width: 300,
            height: 300,
            fit: BoxFit.fill,
          ),
          const SizedBox(height: 50),
          Text(onboardingList[i].title!,
            style: Theme.of(context).textTheme.headline1,),
          const SizedBox(height: 20),
          Container(
            width: double.infinity,
            alignment: Alignment.center,
            child: Text(
              onboardingList[i].body!,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ],
      ),
    );
  }

}