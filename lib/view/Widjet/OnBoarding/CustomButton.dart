import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/Constant/color.dart';
import 'package:library_project/controller/OnBoardingController.dart';

class CustomButtonOnBoarding extends GetView<OnBoardingControllerImp>{
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 80),
      height: 40,
      child: MaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          padding: const EdgeInsets.symmetric(horizontal: 100, vertical: 0),
          textColor: Colors.white,
          onPressed: () {
            controller.next();
          },
          color: Appcolor.primaryColor,
          child:  Text("6".tr,style: TextStyle(fontSize: 15),)),
    );
  }

}
