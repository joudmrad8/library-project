import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/Constant/color.dart';
import 'package:library_project/Data/Datasource/static/static.dart';
import 'package:library_project/controller/OnBoardingController.dart';
import '../Widjet/OnBoarding/CustomButton.dart';
import '../Widjet/OnBoarding/CustomDotController.dart';
import '../Widjet/OnBoarding/CustomSlider.dart';

class onBoarding extends GetView<OnBoardingControllerImp>{
  @override
  Widget build(BuildContext context) {
    Get.put(OnBoardingControllerImp());
    return Scaffold(
      backgroundColor: Colors.white,
        body:
           SafeArea(
             child: Column(
               children: [
                  const Expanded(
                   flex: 3,
                   child:
                   CustomSlider(),
                 ),
                 Expanded(
                     flex:1,
                     child: Column(
                       children:  [
                         CustomDotControllerOnBoarding(),
                         const Spacer(flex:4),
                         CustomButtonOnBoarding()
                    ]  ),
           ),
           ]
          ),
           ),


        );


  }

}