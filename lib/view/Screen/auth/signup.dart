
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/Class/statuscodeReq.dart';
import 'package:library_project/Core/Constant/imageassets.dart';
import 'package:library_project/Core/Function/handlindatacontroller.dart';
import 'package:lottie/lottie.dart';
import '../../../Core/Constant/color.dart';
import '../../../Core/Function/validinput.dart';
import '../../../controller/SignUpController.dart';
import '../../Widjet/auth/custombuttonauth.dart';
import '../../Widjet/auth/textsignup.dart';
import 'package:library_project/Core/Class/crud.dart';

import 'package:http/http.dart' as http;


class SignUp extends StatelessWidget {
   SignUp({Key? key}) : super(key: key);

   SignUpControllerImp controller = Get.put(SignUpControllerImp());
   Crud crud =Get.put(Crud());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            body: GetBuilder<SignUpControllerImp>(builder: (controller)=>
            controller.statusRequest==StatusRequest.loading ?
               Center(child: Lottie.asset(AppImageassets.loading,width: 200,height: 200))
               : Container(
              padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 30),
              child: Form(
                key: controller.formstate,
                child: ListView(

                    physics: BouncingScrollPhysics(),
                    children: [
                      Lottie.asset(AppImageassets.SignupImge,frameRate: FrameRate(5)
                        ,height: 300,width: 270,),
                      const SizedBox(height: 10),
                      Center(child: Text( "8".tr,
                        style: const TextStyle(letterSpacing: 2,fontSize: 30,fontWeight: FontWeight.bold,
                          color: Appcolor.black,),)),
                      const SizedBox(height: 30),

                      TextFormField(
                        controller: controller.username,
                        validator: (val){
                          return validInput(val!, 5, 20, "username");
                        },
                        decoration:InputDecoration(
                          contentPadding:
                          const EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                          hintText:  "10".tr,
                          hintStyle: TextStyle(fontSize: 14),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          label: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 9),
                              child: Text( "9".tr,style: TextStyle(fontWeight: FontWeight.bold,color: Appcolor.black),)),
                          //  labelStyle: TextStyle( color: Appcolor.green,),
                          suffixIcon: Icon( Icons.person_outline,color: Appcolor.black,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(width: 2,color: Appcolor.green)
                          ),
                        ),
                        autovalidateMode:AutovalidateMode.onUserInteraction ,
                      ),
                      SizedBox(height: 20,),

                      TextFormField(
                        controller: controller.email,
                        validator: (val){
                          return validInput(val!, 10, 50, "email");
                        },
                        decoration:InputDecoration(
                          hintText:  "12".tr,
                          contentPadding:
                          const EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                          hintStyle: TextStyle(fontSize: 14),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          label: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 9),
                              child: Text( "11".tr,style: TextStyle(fontWeight: FontWeight.bold,color: Appcolor.black))),
                          //  labelStyle: TextStyle( color: Appcolor.green,),
                          suffixIcon: Icon( Icons.email_outlined,color: Appcolor.black,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(color:Colors.green)
                          ),
                        ),
                        keyboardType:TextInputType.emailAddress,
                        autovalidateMode:AutovalidateMode.onUserInteraction ,
                      ),
                      SizedBox(height: 20,),

                      TextFormField(
                        controller: controller.password,
                        validator: (val){
                          return validInput(val!, 8, 50, "password");
                        },
                        decoration:InputDecoration(
                          contentPadding:
                          const EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                          hintStyle: TextStyle(fontSize: 14),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          label: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 9),
                              child: Text( "13".tr,style: TextStyle(fontWeight: FontWeight.bold,color: Appcolor.black))),
                          hintText:  "14".tr,
                          // labelStyle: TextStyle( color: Appcolor.green,),
                          suffixIcon: Icon( Icons.lock_outline,color: Appcolor.black,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(color: Appcolor.green)
                          ),
                        ),
                        keyboardType:TextInputType.emailAddress,
                        autovalidateMode:AutovalidateMode.onUserInteraction ,
                        //obscureText: true,
                      ),
                      SizedBox(height: 20,),
                      TextFormField(
                        controller: controller.conf_password,
                        validator: (val){
                          return validInput(val!, 8, 50, "password");
                        },
                        decoration:InputDecoration(
                          contentPadding:
                          const EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                          hintStyle: TextStyle(fontSize: 14),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          label: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 9),
                              child: Text( "confirm title".tr,style: TextStyle(fontWeight: FontWeight.bold,color: Appcolor.black))),
                          hintText:  "confirm".tr,
                          // labelStyle: TextStyle( color: Appcolor.green,),
                          suffixIcon: Icon( Icons.lock_outline,color: Appcolor.black,),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(color: Appcolor.green)
                          ),
                        ),
                        keyboardType:TextInputType.emailAddress,
                        autovalidateMode:AutovalidateMode.onUserInteraction ,
                        //obscureText: true,
                      ),
                      const SizedBox(height:  20),
                      CustomButtomAuth(text: "7".tr, onPressed: () {
                        // signupReq(controller.username.text, controller.email.text, controller.password.text,controller.conf_password.text);
                        controller.signUp();
                      }),

                      const SizedBox(height:  20),
                      CustomTextSignUpOrSignIn(
                        textone: "15".tr,
                        texttwo: "16".tr,
                        onTap: () {
                          controller.goToLogin();
                        },
                      ),
                    ]),
              ),
            ),)
    );
  }
}