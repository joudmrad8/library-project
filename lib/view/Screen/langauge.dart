import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../Core/Constant/routes.dart';
import '../../Core/localaization/changelocal.dart';
import '../Widjet/language/custombuttomlang.dart';

class Language extends GetView<LocaleController> {
  const Language({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("1".tr, style: Theme.of(context).textTheme.headline1),
              const SizedBox(height: 30),
              CustomButtonLang(
                  textbutton: "Arabic",
                  onPressed: () {
                    controller.changeLang("ar");
                    Get.toNamed(AppRoute.onboarding) ;
                  }),
              SizedBox(height: 10,),
              CustomButtonLang(
                  textbutton: "English",
                  onPressed: () {
                    controller.changeLang("en");
                    Get.toNamed(AppRoute.onboarding) ;
                  }),
            ],
          )),
    );
  }
}