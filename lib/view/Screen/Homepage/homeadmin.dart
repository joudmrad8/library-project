import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/Constant/color.dart';
import 'package:library_project/controller/HomepageController.dart';
import 'package:library_project/view/Screen/Homepage/favouirte.dart';
import 'package:library_project/view/Screen/Homepage/homepagebody.dart';
import 'package:library_project/view/Screen/Homepage/mybooks.dart';
import 'package:library_project/view/Screen/Homepage/notification.dart';
import 'package:library_project/view/Screen/Homepage/readed.dart';
import 'package:library_project/view/Screen/Homepage/search.dart';
import 'package:http/http.dart' as http;

import '../../../Core/localaization/changelocal.dart';
import '../../../controller/SignUpController.dart';

class HomePageAdmin extends StatefulWidget {
  const HomePageAdmin({super.key});

  @override
  _HomepageAdminState createState() => _HomepageAdminState();
}

class _HomepageAdminState extends State<HomePageAdmin> {
  int currentindex=0;
  bool switcht=false;
  bool en=false;
  bool ar=false;
  String? language;
  bool notify =false;


  List<Widget> screens=[
    MyBooks(),
    Favourite(),
    Homepagebody(),
    Readed(),
    Notifications(),
  ];

  @override
  void initState() {
    currentindex=2;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    HomePageController controllerH = Get.put(HomePageController());
    LocaleController controller = Get.put(LocaleController());
    SignUpControllerImp controllerSi = Get.put(SignUpControllerImp());
    print(controllerSi.username);

    return Scaffold(
        appBar: AppBar(
          // backgroundColor: Appcolor.primaryColor,
          title: const Text("HomePage", style: TextStyle(fontSize: 20),
            textAlign: TextAlign.start,
          ),
          actions: [
            Container(child:
            IconButton( icon: const Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: Search());
              },
            ),),
          ],
        ),
        drawer:  Drawer(
            child: Column(
            children: [
                UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                color: Appcolor.primaryColor,
                ),
                accountName: Container(
                padding: EdgeInsets.symmetric(horizontal: 4,vertical: 10),
                child: Text("${controllerSi.username.text}",style: TextStyle(fontSize: 24),)),
                accountEmail: Text("${controllerSi.email.text}")),
                Container(
                padding: EdgeInsets.only(right: 20),
                child: Column(
                children: [
                ListTile(
                title: const Text("Insert Books",style: TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                onTap: (){},
                leading: Icon(Icons.insert_drive_file_outlined),

                ),
                ListTile(
                title: const Text("Delete Books",style: TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                onTap: (){},
                leading: Icon(Icons.delete_outline_outlined,size: 30),
                ),
                ListTile(
                title: const Text("Update Books",style: TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                onTap: (){},
                leading: Icon(Icons.update_outlined,size: 30),

                ),
                ListTile(
                title: const Text("user's Bonus",style:TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                onTap: (){},
                leading: Icon(Icons.control_point_duplicate_outlined,size: 26),
                ),
                ListTile(
                title: const Text("Borrowed Books ",style:TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                onTap: (){},
                leading: Icon(Icons.library_books,size: 26),
                ),

                SwitchListTile (
                title:const Text("Dark Mode",style: TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                secondary: const Icon(Icons.dark_mode_outlined,size: 30,),
                activeColor: Appcolor.primaryColor,
                // controlAffinity: ListTileControlAffinity.leading
                value: switcht, onChanged:(val)
                {
                setState(() {
                switcht=val;
                });
                }),
                const ListTile(
                title: Text("Language",style: TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                leading: Icon(Icons.language_outlined,size: 30),
                ),

                RadioListTile(
                contentPadding: EdgeInsets.only(right: 40,left: 40),
                title:Text("arabic"),
                activeColor: Appcolor.primaryColor,
                // controlAffinity: ListTileControlAffinity.trailing,
                value: "ar", groupValue: language, onChanged:(value){
                controller.changeLang("ar");
                setState(() {
                language=value!;
                });
            }),
                RadioListTile(title: Text("english"),
                contentPadding: EdgeInsets.only(right: 40,left: 40),
                activeColor: Appcolor.primaryColor,
                // controlAffinity: ListTileControlAffinity.trailing,
                value: "en", groupValue: language, onChanged:(value){
                controller.changeLang("en");
                setState(() {
                language=value!;
                });
                }),
                const ListTile(
                title: Text("Logout",style: TextStyle(fontSize: 16,color: Appcolor.black,fontWeight: FontWeight.normal),),
                leading: Icon(Icons.login_outlined,size: 25),
                ),

              ],
            ), )
            ],),
            ),
    body: screens[currentindex],
    bottomNavigationBar: BottomNavigationBar(
    type: BottomNavigationBarType.fixed,
    elevation: 3,
    currentIndex: currentindex,
    onTap: (index){
    setState(() {
    print (currentindex);
    currentindex=index;
    });
    },
    items:  const [
    BottomNavigationBarItem(
    icon: Icon(Icons.bookmark_add_outlined,),
    label: "My books"
    ),
    BottomNavigationBarItem(
    icon: Icon(Icons.favorite_border_outlined,),
    label: "Favourite"
    ),
    BottomNavigationBarItem(
    icon: Icon(Icons.home_outlined,size: 40,),
    label: "Home"

    ),
    BottomNavigationBarItem(
    icon: Icon(Icons.playlist_add_check_outlined,),
    label: "Readed"
    ),
    BottomNavigationBarItem(
    icon: Icon(Icons.notifications_none,),
    label: "Notification"
    ),

    ],
    )
    );
    }
}