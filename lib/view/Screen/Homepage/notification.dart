import 'package:flutter/material.dart';

class Notifications extends StatelessWidget{
  const Notifications({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: const Padding(
          padding: EdgeInsets.only(top: 200,bottom: 200),
          child: Center(
            child: Column(
              children: [
                Icon(
                  Icons.notifications_none_outlined,size: 100,
                ),
                Text("Don't have any notification yet")
              ],
            ),
          ),
        )
    );
  }

}