import 'package:flutter/material.dart';
import 'package:get/get_connect/sockets/src/socket_notifier.dart';
import 'package:library_project/Core/Constant/color.dart';
import 'package:library_project/Core/Constant/imageassets.dart';
import 'package:lottie/lottie.dart';


class Search extends SearchDelegate{
  List <String> names=[
    "joudi hamdan",
    "atomic Habites",
    "ahmad al omary",
    "adham shirqawy"
  ];

  @override
  List<Widget>? buildActions(BuildContext context) {
          return [
            IconButton(onPressed: (){
              query="";
             },
                icon: Icon(Icons.close,color: Colors.orange[200],size: 30,))
          ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: (){
      close(context, null);
    },
     icon: Icon(Icons.arrow_back,size: 30,color: Colors.orange[200],));
  }

  @override
  Widget buildResults(BuildContext context) {
         return Container(
         //  padding: EdgeInsets.symmetric(horizontal: 30,vertical: 20),
                width: double.infinity,
                //margin: EdgeInsets.all(40),
               decoration: BoxDecoration(
                 color:Colors.deepPurple[100],
                  shape: BoxShape.rectangle
               ),
                 child: Center(child: Text(query,
                   style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,letterSpacing: 1),)));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
         //element has the names[] content

    List filter=names.where((element) => element.contains(query)).toList();
        return query==""
        ?
            Center(heightFactor: 1.5,
              child: Lottie.asset(AppImageassets.searchImge,width: 350,height: 350),)
        :
            ListView.builder(
                itemCount:  query=="" ? names.length : filter.length,
                itemBuilder: (context,i){

                  return InkWell(
                    borderRadius: BorderRadius.circular(10),
                    onTap: (){
                      query= query=="" ? names[i] : filter[i];
                       showResults(context);
                    },
                    splashColor: Colors.orange[200],
                    child: Container(
                       margin: EdgeInsets.only(bottom: 5),
                       padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(20),
                        ) ,

                       child:
                        Text("${filter[i]}",
                      style: TextStyle(fontSize: 20),
                      ),
                    ),
                  );
              });


  }

}


















//
// class Search extends StatelessWidget{
//   const Search({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//    return Scaffold(
//      body: Padding(
//        padding:
//        const EdgeInsets.only(top: 50,left: 20,right: 20),
//        child: Container(
//          padding: EdgeInsets.only(top: 20,left: 2,right: 2),
//          child: SingleChildScrollView(
//            physics: BouncingScrollPhysics(),
//            child: Column(
//              children: [
//                TextFormField(
//
//                  //scrollPadding:  EdgeInsets.all(20),
//                  decoration:  InputDecoration(
//                    suffixIcon: Container(
//                      padding: EdgeInsets.all(15),
//                      decoration:BoxDecoration(
//                          color: Appcolor.primaryColor,
//                        borderRadius: BorderRadius.circular(30)
//                      ),
//                     child: Icon(Icons.search,color:Colors.white),),
//                      hintText: "Enter your book name",
//                      border:  OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(30),
//                        borderSide: BorderSide(color: Appcolor.primaryColor)
//                    ),
//                    focusedBorder: OutlineInputBorder(
//                        borderRadius: BorderRadius.circular(30),
//                      borderSide: BorderSide(color: Appcolor.primaryColor,width: 2)
//                    ),
//                    ),
//
//                ),
//
//              Center(heightFactor: 1.5,
//                child: Lottie.asset(AppImageassets.searchImge),),
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//   }
//
// }