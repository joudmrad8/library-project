import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/Constant/color.dart';
import 'package:library_project/Core/Function/handlindatacontroller.dart';
import 'package:library_project/controller/HomepageController.dart';

import '../../../Core/Class/handlingview.dart';

class Homepagebody extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
  Get.put(HomePageController());
  print("hi");
  return Scaffold(
      body: GetBuilder<HomePageController>(
        builder: (controller)=>HandlingDataView(
            statusRequest: controller.statusRequest,
            widget:Container(
                child:ListView.builder(
                  scrollDirection: Axis.vertical,
                   itemCount: controller.data.length,
                    itemBuilder:(context,index){
                    return Container(
                      color: Appcolor.primaryColor,
                      child: Text("${controller.data}"),
                    );
                    }
                )

            )
        ),


      ),
    );
  }

}