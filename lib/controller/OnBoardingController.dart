import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:library_project/Data/Datasource/static/static.dart';

import '../Core/Constant/routes.dart';

abstract class OnBoardingController extends GetxController{
  next();
  onPageChanged(int index);
}
class OnBoardingControllerImp extends OnBoardingController {
  late PageController pageController;

  int currentPage = 0;

  @override
  next() {
    currentPage++;

    if (currentPage > onboardingList.length - 1) {
      Get.offAllNamed(AppRoute.signup) ;
    } else {
      pageController.animateToPage(currentPage,
          duration: const Duration(milliseconds: 800), curve: Curves.easeInOut);
    }
  }

  @override
  onPageChanged(int index) {
    currentPage = index;
    update();
  }

  @override
  void onInit() {
    pageController = PageController();
    super.onInit();
  }
}