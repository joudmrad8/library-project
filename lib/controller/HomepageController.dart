import 'package:get/get.dart';
import 'package:library_project/Core/Constant/routes.dart';
import 'package:library_project/Core/Services/services.dart';
import 'package:library_project/Data/Datasource/remote/home_data.dart';

import '../Core/Class/statuscodeReq.dart';
import '../Core/Function/handlindatacontroller.dart';

class HomePageController extends GetxController{

    @override
  void onInit() {
    getData();
    print("homepage");
    super.onInit();
  }

MyServices myServices=Get.find();

List data = [];
HomeData homeData = HomeData(Get.find());
late StatusRequest statusRequest;

getData() async {
    statusRequest = StatusRequest.loading;
    var response = await homeData.getData();
    print("=============================== Controller $response ") ;
    statusRequest = handlingData(response);
    if (StatusRequest.success == statusRequest) {
      data.addAll(response);
      print(data);
    }
    update();
}




}