import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:library_project/Core/Class/crud.dart';
import 'package:library_project/Core/Class/statuscodeReq.dart';
import '../Core/Constant/routes.dart';
import '../Core/Function/handlindatacontroller.dart';
import '../Data/Datasource/remote/auth/signup.dart';


class SignUpControllerImp extends GetxController {

  GlobalKey<FormState> formstate = GlobalKey<FormState>();

  late TextEditingController username;
  late TextEditingController email;
  late TextEditingController password;
  late TextEditingController conf_password;


  SignupData signData = SignupData(Get.find());
  Crud crud =Get.put(Crud());

  List data = [];

  StatusRequest? statusRequest;
  signUp() async {
    if (formstate.currentState!.validate()) {
      statusRequest = StatusRequest.loading;
      update();
      var response = await signData.PostData(username.text, email.text, password.text,  conf_password.text);
      print("=============================== Controller $response ") ;
      statusRequest = handlingData(response);
      if (StatusRequest.success == statusRequest) {
      //  data.addAll(response['data']);
        isadmin();
      }else {
        Get.defaultDialog(title: "warning",middleText: "email or username already exists");
        statusRequest=StatusRequest.failure;
      }
      update();
    } else {

    }
  }

  goToLogin() {
    Get.toNamed(AppRoute.login);
  }

  @override
  void onInit() {
    username = TextEditingController() ;
    email = TextEditingController();
    password = TextEditingController();
    conf_password = TextEditingController();

    super.onInit();
  }

  @override
  void dispose() {
    username.dispose();
    email.dispose();
    password.dispose();
    conf_password.dispose();
    super.dispose();
  }
  isadmin(){
    if(username.text=="ADMIN" && email.text=="Admin1@gmail.com" ) {
      Get.toNamed(AppRoute.homepagead);}
    else{ Get.toNamed(AppRoute.homepage);}
  }

}